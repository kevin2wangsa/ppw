$(document).ready(function () {
    $('#accordion').accordion({
        collapsible: true,
        active: 0,
        height: 'fill',
        header: 'h3',
        animate: 400,
        event: "click"
        
    }).sortable({
        items: '.item'
    });
    
    $('#accordion').on('accordionactivate', function (event, ui) {
        if (ui.newPanel.length) {
            $('#accordion').sortable('disable');
        } else {
            $('#accordion').sortable('enable');
        }
        });
    });
    
    $(document).ready(function () {
    $('.move-down').click(function (e) {
        var self = $(this),
            item = self.parents('div.item'),
            swapWith = item.next();
        item.before(swapWith.detach());
    });
    
    $('.move-up').click(function (e) {
        var self = $(this),
            item = self.parents('div.item'),
            swapWith = item.prev();
        item.after(swapWith.detach());
    });
    });

    $("#keyworrd").keyup(function() {
        var ketikan = $("#keyword").val();
        console.log(ketikan);

        $.ajax({
            url : 'https://www.googleapis.com/books/v1/volumes?q=' + ketikan,
            success : function(data)  {
                var array_items = data.items;
                 
                 $("daftar_isi").empty();
                 for ( i = 0 ; i <array_items.length;i++){
                     var judul = array_items[i].volumeInfo.title;
                     var gambar = array_items[i].volumeInfo.imageLinks.smallThumbail;

                    $("daftar_isi").append("<li>" + judul + "<br><img src= " + gambar + "></li>" );
                    
                 }
            }     
        });

    });
