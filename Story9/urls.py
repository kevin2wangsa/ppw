from django.urls import path
from . import views

app_name = 'Story9'

urlpatterns = [
    path('', views.main,name='main'),
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logoutUser, name='logout'),
    path('register/', views.registerPage, name='register'),

]