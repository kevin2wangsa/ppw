from django.urls import path
from . import views

app_name = 'Story8'

urlpatterns = [
    path('', views.ajax,name='ajax'),
    path('regis', views.regis,name='regis'),
]