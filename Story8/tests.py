from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest, response

from django.apps import apps
from Story8.apps import Story8Config


class Test(TestCase):
    def ajax_is_exist(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(Story8Config.name, 'Story8')
        self.assertEqual(apps.get_app_config('Story8').name, 'Story8')






