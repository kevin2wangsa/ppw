from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages

# Create your views here.

def ajax(request):
    return render(request, 'home/ajax.html')

def regis(request):
    return render(request, 'home/depan.html')
