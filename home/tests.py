from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest, response

from .models import Kegiatan,Peserta

# Create your tests here.
class Story6(TestCase):

    #url-test
    def card_kegiatan_is_exist (self) :
        response = Client().get('/card_kegiatan')
        self.assertEqual(response.status_code, 200)

    def tambah_is_exist(self):
        response = Client().get('/tambah')
        self.assertEqual(response.status_code, 200)

    def tambah_peserta_is_exist(self):
        response = Client().get('/tambah_peserta')
        self.assertEqual(response.status_code, 200)

    #302 jangan lupa
    
    #view-test
    def tombol_tambah_is_exist(self):
        response = Client().get('/card_kegiatan')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Tambah Aktifitas", html_kembalian)

    #model-test
    def test_models(self):
        Kegiatan.objects.create(namaKegiatan='Programing')
        counting = Kegiatan.objects.all().count()
        self.assertAlmostEquals(counting, 1)
