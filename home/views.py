from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404

from .forms import ProductForm , KegiatanForm , PesertaForm
from .models import Product , Kegiatan , Peserta

# Create your views here.

def index(request):
    return render(request, 'home/index.html')

def Projek(request):
    return render(request, 'home/Projek.html')

def blog(request):
    return render(request, 'home/blog.html')

def kegiatan(request):
    return render(request, 'home/kegiatan.html')

def about(request):
    return render(request, 'home/about.html')

def list(request):
    listp = Product.objects.all()
    return render(request, 'home/product_list.html' , {'listp':listp})

def product_create_view(request):
    form = ProductForm()
    if request.method == 'POST':
        form = ProductForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/ListProduct')
    return render(request, 'home/product_create.html', {'form':form})

def delete(request,id):
    item = Product.objects.get(id=id)
    if request.method == 'POST':
        item.delete()
        return redirect('/ListProduct')
    return render(request, 'home/product_delete.html', {'item':item})

def detail(request,id):
    prod = Product.objects.get(id=id)
    return render (request, 'home/product_detail.html',{'prod':prod})

def update(request, id):
    prod = Product.objects.get(id=id)
    form = ProductForm(instance=prod)
    if request.method == 'POST':
        form = ProductForm(request.POST, instance=prod)
        if form.is_valid():
            form.save()
            return redirect('/ListProduct')

    return render(request, 'home/product_create.html', {'form':form})

def Kegiatan_create(request):
    keg = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    keg_par = []

    for kegiatan in keg:
        kegiatan_partic = []

        for participant in peserta:
            if (str(participant.kegiatan) == str(kegiatan)):
                kegiatan_partic.append(participant)
        keg_par.append((kegiatan,kegiatan_partic))
        
    context= {
        'keg':keg,
        'keg_par' : keg_par,
        
    }
    return render(request, 'home/card_kegiatan.html', context)

def tambah(request):
    form = KegiatanForm()
    if request.method == 'POST':
        form = KegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/Kegiatan')
    return render(request, 'home/kegiatan.html', {'form': form})


def tambah_peserta(request):
    form = PesertaForm()
    if request.method == 'POST':
       form = PesertaForm(request.POST)
       if form.is_valid():
           form.save()
           return redirect('/Kegiatan')
    return render(request, 'home/form_peserta.html',{'form':form})
