from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [

    #Path-Interface
    path('', views.index,name='index'),
    path('Blog/', views.blog,name='blog'),
    path('Projek/', views.Projek,name='projek'),
    path('Kegiatan/', views.kegiatan,name='kegiatan'),   
    path('About/',views.about,name='about'),   

    #View-Mata-Kuliah
    path('FormProduct',views.product_create_view, name= 'product_create'),
    path('ListProduct', views.list, name='product_list'),
    path('delete/<str:id>', views.delete, name="product_delete"),
    path('<int:id>',views.detail),
    path('update/<str:id>', views.update, name='update'),

    #View-Kegiatan
    path('Kegiatan', views.Kegiatan_create, name='card_kegiatan'),
    path('tambah', views.tambah, name='tambah'),
    path('tambah_peserta/', views.tambah_peserta, name='tambah_peserta'),

]