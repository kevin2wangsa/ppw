from django.contrib import admin
from .models import Product , Kegiatan , Peserta

# Register your models here.
admin.site.register(Product)
admin.site.register(Kegiatan)
admin.site.register(Peserta)

