from django.db import models

# Create your models here.
class Product(models.Model):
    namaMatkul = models.CharField(max_length = 30)
    dosen      = models.CharField(max_length = 30)
    jumlahSks  = models.IntegerField()
    semester   = models.IntegerField()
    ruangKelas = models.CharField(max_length = 6)

    def __str__(self):
        return self.namaMatkul

class Kegiatan(models.Model):
    namaKegiatan = models.CharField(max_length = 200)

    def __str__(self):
        return self.namaKegiatan

class Peserta(models.Model):
    namaPeserta = models.CharField(max_length = 200)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.kegiatan)+ "dan" + self.namaPeserta

