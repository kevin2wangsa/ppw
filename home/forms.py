from django import forms

from .models import Product , Kegiatan , Peserta 

class ProductForm(forms.ModelForm):
    namaMatkul = forms.CharField(label=' Nama Matkul : ')
    dosen      = forms.CharField(label=' Nama Dosen   : ')
    jumlahSks  = forms.IntegerField(label=' Jumlah SKS : ')
    semester   = forms.CharField(label=' Semester  : ')
    ruangKelas = forms.CharField(label=' Ruang Kelas : ')
    
    class Meta:
        model = Product
        fields = '__all__'

class KegiatanForm(forms.ModelForm):
    namaKegiatan = forms.CharField(label='Nama Kegiatan : ')

    class Meta:
        model = Kegiatan
        fields = '__all__'
    
class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = '__all__'

