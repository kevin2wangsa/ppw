$(document).ready(function () {
    $('#accordion').accordion({
        collapsible: true,
        active: 0,
        height: 'fill',
        header: 'h3',
        animate: 400,
        event: "click"
        
    }).sortable({
        items: '.item'
    });
    
    $('#accordion').on('accordionactivate', function (event, ui) {
        if (ui.newPanel.length) {
            $('#accordion').sortable('disable');
        } else {
            $('#accordion').sortable('enable');
        }
        });
    });
    
    $(document).ready(function () {
    $('.move-down').click(function (e) {
        var self = $(this),
            item = self.parents('div.item'),
            swapWith = item.next();
        item.before(swapWith.detach());
    });
    
    $('.move-up').click(function (e) {
        var self = $(this),
            item = self.parents('div.item'),
            swapWith = item.prev();
        item.after(swapWith.detach());
    });
    });
    